# Copyright (C) 2018 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

import os

from lib.cuckoo.common.abstracts import Signature

class McafeeWarning(Signature):
    name = "mcafee_warning"
    description = "warning from Mcafee"
    authors = ["giaespo"]
    severity = 50
    categories = ["generic"]
    minimum = "2.0"

    def on_call(self, call, process):
        if "warning.txt" in process["command_line"].lower():
            self.mark_call()

    def on_complete(self):
        return self.has_marks()
