# Author: Francesco Perna

import logging
from oletools.rtfobj import RtfObjParser

from cuckoo.common.abstracts import Extractor

# log = logging.getLogger("cuckoo")


class RTFShellcode(Extractor):
    yara_rules = "RTFShellcode"
    # yara_rules = ["exploit_ole_stdolelink", "RTFShellcode"]
    minimum = "2.0.5"

    #def __init__(self, parent):
    #    self.parent = parent
    #    print "ASDASJDKLFASJDKL ASJ DKLAJSKLD JASKL DJASKLDJ ASKLDJASLDJ ASLKJ DASKLD ASKLD"
    #    log.error("PAAAAAASDJIASJDASIODJASIODJASIODASDIJ ASDIJ ASDIOASJDOI ASDIOAS")

    def handle_yara(self, filepath, match):
        rtf_stream = open(filepath, 'rb').read()
        rtfp = RtfObjParser(rtf_stream)
        rtfp.parse()

        for rtf_extracted in rtfp.objects:
            self.push_blob(rtf_extracted.rawdata, "binaries", None, {})

            # "filename": rtfobj.start.decode("latin-1"),
            # "src_path": rtfobj.src_path.decode("latin-1"),
            # "temp_path": stream.temp_path.decode("latin-1"),
