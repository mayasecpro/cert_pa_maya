# Copyright (C) 2016 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature

class IpBlackList(Signature): 			# We initialize the class inheriting Signature.\par
    name = "IpBlackList" 			# We define the name of the signature\par
    description = "IP blacklisted" 		# We provide a description\par
    severity = 50 				# We set the severity to maximum\par
    categories = "network" 			# We add a category\par
    authors = ["Alessandro Gaglia"] 		# We specify the author\par

def run(self):
    return self.check_ip("10.1.26.181")
