# Copyright (C) 2016 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class ForbiddenIp(Signature):
    name = "forbidden_ip"
    severity = 50
    categories = ["network"]
    authors = ["Francesco Perna"]
    minimum = "2.0"
    description = "Forbidden IP and URL submitted"
    forbidden_ips = ["http://10.1.26.181",]

    def on_complete(self):

        # "target": {
        #     "category": "url",
        #     "url": "http://10.1.26.181:8080?rid=TswMV8b"
        # }

        category = self.get_results("target", {}).get("category")

        if category == "url":
            target = self.get_results("target", {}).get("url")

            for ip in self.forbidden_ips:
                if target.startswith(ip):
                    self.mark(
                        forbidden_ips=ip,
                    )

                    return self.has_marks()
