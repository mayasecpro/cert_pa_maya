# Copyright (C) 2016 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class HoneypotDns(Signature):
    name = "honeypot_dns"
    severity = 50
    categories = ["network"]
    authors = ["Francesco Perna"]
    minimum = "2.0"
    description = "Honeypot blocked domains"

    def on_complete(self):
        # Slightly hacky but will have to do for now.

        # [
        #     {'ip': '10.1.26.180', 'domain': 'h8rs.org'},
        #     {'ip': '2.228.46.121', 'domain': 'www.download.windowsupdate.com'}
        # ]

        domains_to_check = self.get_net_domains()

        for domain in domains_to_check:
            print(domain)
            if domain["ip"] == "10.1.26.180" or domain["ip"] == "0.0.0.0":
                self.mark(
                    domain=domain["domain"],
                )

        return self.has_marks()
