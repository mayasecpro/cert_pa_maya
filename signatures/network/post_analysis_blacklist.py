# Copyright (C) 2016 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class PostAnalysisBlacklist(Signature):
    name = "post_analysis_blacklist"
    severity = 50
    categories = ["network"]
    authors = ["Francesco Perna"]
    minimum = "2.0"
    description = "Post Analysis blacklisted IP"
    ip_list = ["66.6.33.149","10.1.26.181","208.70.234.231","192.185.3.209", "91.218.127.106","185.189.149.183","103.39.78.143","89.223.28.184"]

    def on_complete(self):
        # Slightly hacky but will have to do for now.

        # [
        #     {'ip': '10.1.26.180', 'domain': 'h8rs.org'},
        #     {'ip': '2.228.46.121', 'domain': 'www.download.windowsupdate.com'}
        # ]

        domains_to_check = self.get_net_domains()

        for domain in domains_to_check:
            print(domain)
            if domain["ip"] in self.ip_list:
                self.mark(
                    ip=domain["ip"],
                )

        return self.has_marks()
