import ntpath

from lib.cuckoo.common.abstracts import Signature

network_objects = [
    "microsoft.xmlhttp",
    "msxml2.serverxmlhttp",
    "msxml2.xmlhttp",
    "msxml2.serverxmlhttp.6.0",
    "winhttp.winhttprequest.5.1",
]

class OfficeCertutil(Signature):
    name = "office_certutil"
    description = "Office invokes certutil (possible abuse)"
    severity = 3
    categories = ["vba", "tool abuse"]
    authors = ["FDD @ Cuckoo Technologies"]
    minimum = "2.0"

    filter_apinames = "vbe6_Shell", "vbe6_Invoke"

    def on_call(self, call, process):
        cmd = None
        if "command_line" in call["arguments"]:
            cmd = call["arguments"]["command_line"]
        elif "args" in call["arguments"]:
            args = filter(lambda item: isinstance(item, basestring), call["arguments"]["args"])
            cmd = "".join(args)

        if cmd and "certutil" in cmd.lower() and "-decode" in cmd.lower():
            self.mark_call()
            return True

class OfficeExec(Signature):
    name = "office_exec"
    description = "Office document executes commands or files"
    severity = 5
    categories = ["vba"]
    authors = "FDD @ Cuckoo Sandbox"
    minimum = "2.0"
    filter_apinames = "vbe6_Invoke"

    def on_call(self, call, process):
        if (call["arguments"]["funcname"] != "Exec" and
            call["arguments"]["funcname"] != "Run"):
            return

        self.mark_call()
        cmd = call["arguments"]["args"][0]
        self.mark_ioc("cmd", cmd)
        return True
