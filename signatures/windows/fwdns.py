# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature

class FWDNS(Signature):
    name = "fwds_match"
    description = "Match FWDNS Blacklist"
    severity = 10
    categories = ["blocklist"]
    authors = ["nwn"]
    minimum = "2.0.3"

    ipaddrs = [
        "10.1.26.180",
    ]

    def on_complete(self):
        for indicator in self.ipaddrs:
            if self.check_ip(pattern=indicator):
                self.mark_ioc("ipaddr", indicator)
                return True

