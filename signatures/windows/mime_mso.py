# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class MimeMSO(Signature):

    name = "mime mso"
    description = "mime mso macro vba"
    severity = 5
    categories = ["script", "malware", "office", "macro"]
    authors = ["Kalival"]

    def on_yara(self, category, filepath, match):

        if (match.name == "mime_mso_embedded_ole" and category == "sample") or (match.name == "mime_mso_vba_macros" and category == "sample"):

            self.mark_config({
                "family": "mime mso",
                })
            return True
        return