# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class OfficeMSI(Signature):

    name = "Downloader msi installer"
    description = "Download msi installer in macro VBA Office"
    severity = 5
    categories = ["script", "malware", "office", "macro"]
    authors = ["Kalival"]

    def on_yara(self, category, filepath, match):

        if match.name == "OfficeMSI" and category == "sample":

            self.mark_config({
                "family": "Trojan in macro Office",
                })
            return True
        return
