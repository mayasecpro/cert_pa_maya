# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class HtmlRedirect(Signature):

    name = "html_redirect"
    description = "Phish redirect page"
    severity = 5
    categories = ["script", "malware", "phishing"]
    authors = ["Alessandro Gaglia", "Malwerio"]

    def on_yara(self, category, filepath, match):

        if match.name == "HtmlRedirect" and category == "sample":

            self.mark_config({
                "family": "HTML Redirect",
                })
            return True
        return
