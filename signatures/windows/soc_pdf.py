
# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature


class SuspiciousPdf(Signature):

    name = "suspicious_pdf"
    description = "Phish PDF link redirect page"
    severity = 5
    categories = ["script", "malware", "phishing"]
    authors = ["Alessandro Gaglia"]

    def on_yara(self, category, filepath, match):

        if match.name == "suspicious_pdf":

            self.mark_config({
                "family": "Suspicious PDF",
            })
            return True
        return
