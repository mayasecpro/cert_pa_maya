# Copyright (C) 2015-2017 Cuckoo Foundation
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

import re

from lib.cuckoo.common.abstracts import Signature

class SuspiciousPowershellSoc(Signature):
    name = "suspicious_powershell_soc"
    description = "Creazione di un processo Powershell sospetto"
    severity = 50
    categories = ["script", "dropper", "downloader", "packer"]
    authors = ["Alessandro Gaglia"]
    minimum = "2.0"

    def on_complete(self):
        for cmdline in self.get_command_lines():
            lower = cmdline.lower()

            if "powershell" not in lower:
                continue

            envre = re.compile("\$env:temp +")
            m = envre.search(lower)
            if m:
                self.mark(value="Attempts to execute command in temp folder", option=m.group(0))

            urlre = re.compile("https?:\/\/[a-z0-9.-/,'?$\"]+")
            m = urlre.search(lower)
            if m:
                self.mark(value="Attempts to execute command to connect URL", option=m.group(0))

            hiddenre = re.compile("\-[w^]{1,2}[indowstyle^]*[\s]+h\"?i\"?d\"?d\"?e\"?n\"?")
            m = hiddenre.search(lower)
            if m:
                self.mark(value="Tentativi di esecuzione di un comando in finestra nascosta", option=m.group(0))
        
	return self.has_marks()

