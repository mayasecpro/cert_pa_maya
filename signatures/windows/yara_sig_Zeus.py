
# Copyright (C) 2017 Cuckoo Foundation.
# This file is part of Cuckoo Sandbox - http://www.cuckoosandbox.org
# See the file 'docs/LICENSE' for copying permission.

from lib.cuckoo.common.abstracts import Signature

class zeus(Signature):
    name = "Zeus_check"
    description = "Zeus_yara has been matched"
    severity = 2
    categories = ["malware", "worm"]
    authors = ["FDD", "Cuckoo Technologies"]
    minimum = "2.0.4"

    def on_yara(self, category, filepath, match):
        if match.name != "MALW_Zeus":
            return

        self.mark_config({
            "family": "malware",
            
        })
        return True
