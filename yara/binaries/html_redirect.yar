rule HtmlRedirect {

  meta:
        author = "Kalival"
        date = "2019-01-31"
        description = "html method to redirect page"
  strings:
	
	// automatic redirect in HTML
        $h0 = "refresh" nocase
        $h1 = "<meta http-equiv=" nocase
	$h2 = { 3C 68 74 6D 6C 3E }
	
  condition:
        all of them
}

