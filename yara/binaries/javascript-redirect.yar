rule JavascriptRedirect {

  meta:
        author = "Kalival"
        date = "2019-01-31"
        description = "Javascript method to redirect page"
  strings:
	
	// automatic redirect in javascript
	$j0 = "<script" nocase
	$j1 = "location" nocase
	$j2 = "</script>" nocase

  condition:
        all of them
}

