rule  Pdf_withJS : PDF debug
{
        meta:
                copyright = "Neverwinter"
                description = "Detects PDF files with an JavaScript "

        strings:
                $magic = { 25 50 44 46 }
                $object = /\/JavaScript /
        condition:
                all of them
}
