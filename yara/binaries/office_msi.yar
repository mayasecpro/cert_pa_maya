rule OfficeMSI {
   meta:
      description = "Trojan in macro of Office suite: msi installer downloader in macro"
      authors = "kalival"
      date = "2019-03-03"
   strings:

      $h0 = "msiexec" nocase
      $h1 = /https?:\/\// nocase

      $s0 = { 50 4B 03 04 14 00 06 00 } // extension for DOCX, PPTX, XLSX  
      $s1 = { D0 CF 11 E0 A1 B1 1A E1 } // extension for xls,doc,ppt,msg


   condition:
      (all of ($h*)) and (($s0 at 0) or ($s1 at 0))
}

