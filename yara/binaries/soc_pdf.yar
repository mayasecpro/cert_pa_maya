rule suspicious_pdf : PDF
{
	meta:
		author = "Alessandro Gaglia"
		version = "0.1"
		weight = 4
		
	strings:
		$magic = { 25 50 44 46 }
		$a1 = "chempion"
                $a2 = "Softplicity"
                $a3 = /\(http.+\)/

	condition:
		$magic at 0 and 1 of ($a*)
}


