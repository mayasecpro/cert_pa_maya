rule VBScriptRedirect {

  meta:
        author = "Kalival"
        date = "2019-02-28"
        description = "VBScript method to redirect page"
  strings:

	//redirect in VBS Script
	$s0 = "window.open" nocase
  $s1 = "<script" nocase
  $s2 = "</script>" nocase
  $s3 = "vbscript" nocase
	
  condition:
        all of them
}

