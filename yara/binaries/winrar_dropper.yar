rule winrar_dropper {

  meta:
        author = "Kalival"
        date = "2019-02-27"
        description = "Exploit of the WinRAR vulnerability: drop executables"
  strings:
	
	$s0 = "\\Startup\\" nocase
        $s1 = ".exe" nocase

	$m0 = "ace" nocase
	$m1 = { 52 61 72 21 1A 07 00 } // Rar!... version 1.50 onward
	$m2 = { 52 61 72 21 1A 07 01 00 } // Rar!.... version 5.0 onward

  condition:

	$s0 and $s1 and 1 of ($m*)
        
}
