rule certificate_payload
{
    strings:
        $re1 = /-----BEGIN CERTIFICATE-----\r\n[^M]/
 
    condition:
        $re1 at 0
}

rule embedded_certificate_payload
{
    strings:
        $re1 = /-----BEGIN CERTIFICATE-----\r\n[^M]/
 
    condition:
        @re1 > 0
}
